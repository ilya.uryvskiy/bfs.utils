﻿using System;

namespace DwCapp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter window classname -> ");
            var className = Console.ReadLine()?.Trim();
            if (string.IsNullOrWhiteSpace(className)) className = null;
            
            
            Console.Write("Enter window name -> ");
            var name = Console.ReadLine()?.Trim();
            if (string.IsNullOrWhiteSpace(name)) name = null;


            var windowHandle = WinApi.FindWindow(className, name);

            if (windowHandle == IntPtr.Zero)
            {
                Console.WriteLine("No window found...");
                return;
            }

            Console.Write("Message -> ");
            int message;
            int.TryParse(Console.ReadLine()?.Trim(), out message);
            
            Console.Write("WParam -> ");
            int wParam;
            int.TryParse(Console.ReadLine()?.Trim(), out wParam);
            
            Console.Write("LParam -> ");
            int lParam;
            int.TryParse(Console.ReadLine()?.Trim(), out lParam);

            WinApi.SendMessage(windowHandle, message, new IntPtr(wParam), new IntPtr(lParam));
            Console.WriteLine("Message sent, press [enter] to exit");
            Console.ReadLine();
        }
    }
}
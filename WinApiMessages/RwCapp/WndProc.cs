﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

namespace RwCapp
{
    public class WndProc
    {
        public delegate bool WndProcHandler(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

        private readonly WndProcHandler _wndProcHandler;
        private volatile IntPtr _wndHandle;
        private Thread _thread;

        public WndProc(WndProcHandler wndProcHandler)
        {
            _wndProcHandler = wndProcHandler;
        }

        public IntPtr CreateWindow(string className, string name)
        {
            var createWndHandleEvent = new ManualResetEvent(false);

            _thread = new Thread(() =>
            {
                try
                {
                    Run(className, name, createWndHandleEvent);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex);
                }
            });

            _thread.IsBackground = true;
            _thread.Start();

            createWndHandleEvent.WaitOne();
            return _wndHandle;
        }

        private void Run(string className, string name, ManualResetEvent resetEvent)
        {
            var wndClassEx = new WinApi.WNDCLASSEX
            {
                cbSize = Marshal.SizeOf(typeof(WinApi.WNDCLASSEX)),
                style = 0,
                lpfnWndProc = Marshal.GetFunctionPointerForDelegate<WinApi.WndProc>((hWnd, msg, wParam, lParam) =>
                {
                    if (_wndProcHandler?.Invoke(hWnd, (int)msg, wParam, lParam) ?? false)
                    {
                        return IntPtr.Zero;
                    }
                    return WinApi.DefWindowProc(hWnd, msg, wParam, lParam);
                }),
                cbClsExtra = 0,
                cbWndExtra = 0,
                hInstance = Process.GetCurrentProcess().Handle,
                hIcon = IntPtr.Zero,
                hCursor = IntPtr.Zero,
                hbrBackground = IntPtr.Zero,
                lpszMenuName = null,
                lpszClassName = className,
                hIconSm = IntPtr.Zero
            };

            var regClassResult = WinApi.RegisterClassEx(ref wndClassEx);

            if (regClassResult == 0)
            {
                throw new Win32Exception();
            }

            _wndHandle = WinApi.CreateWindowExW(0,
                className,
                name,
                0,
                0,
                0,
                0,
                0,
                IntPtr.Zero,
                IntPtr.Zero,
                wndClassEx.hInstance,
                IntPtr.Zero);


            if (_wndHandle == IntPtr.Zero)
            {
                throw new Win32Exception();
            }

            resetEvent.Set();

            while (WinApi.GetMessage(out var msg, _wndHandle, 0, 0) != 0)
            {
                WinApi.TranslateMessage(ref msg);
                WinApi.DispatchMessage(ref msg);
            }
        }

        public void CloseWindow()
        {
            WinApi.SendMessage(_wndHandle, WinApi.WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
        }
    }
}
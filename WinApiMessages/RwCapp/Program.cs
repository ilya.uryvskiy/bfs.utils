﻿using System;

namespace RwCapp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter window classname -> ");
            var className = Console.ReadLine()?.Trim();
            if (string.IsNullOrWhiteSpace(className)) className = null;
            
            
            Console.Write("Enter window name -> ");
            var name = Console.ReadLine()?.Trim();
            if (string.IsNullOrWhiteSpace(name)) name = null;
            
            
            var wndProc = new WndProc((IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam) =>
            {
                Console.WriteLine($"Received a message => Message: {msg}\tWParam: {wParam.ToInt64()}\tLParam: {lParam.ToInt64()}");
                return false;
            });

            Console.WriteLine($"Creating a window with class name {className} and name {name}");
            Console.WriteLine("Press [enter] to proceed, to exit press [enter] again");
            Console.ReadLine();
            wndProc.CreateWindow(className, name);
            Console.ReadLine();
            wndProc.CloseWindow();
        }
    }
}